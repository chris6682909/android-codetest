package com.example.androidcodetest.Models

data class Result(
    val resultCount: Int,
    val results: List<Music>
)