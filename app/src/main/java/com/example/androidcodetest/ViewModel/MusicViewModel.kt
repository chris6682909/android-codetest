package com.example.androidcodetest.ViewModel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.MutableLiveData
import com.example.androidcodetest.Models.Result
import com.example.androidcodetest.Models.Music
import com.example.androidcodetest.Repository.MusicRepository
import kotlinx.coroutines.launch
import retrofit2.Response
import java.util.stream.Collectors

class MusicViewModel: ViewModel() {

    private val repository = MusicRepository()

    private val cache_list = MutableLiveData<Result>()
    private val muta_music = MutableLiveData<Result>()
    public var live_result: LiveData<Result> = muta_music

    var searchText by mutableStateOf("")

    val selectedOption = mutableStateOf("1")

    fun filterMusic() {
        if(searchText.isNullOrEmpty()){
            muta_music.postValue(
                Result(
                    resultCount = cache_list.value!!.resultCount,
                    results = cache_list.value!!.results
                )
            )

            return
        }

        if (cache_list != null) {

            var list: List<Music> = cache_list.value!!.results


            var filter: List<Music> =
                list.stream().filter { mus: Music ->
                    mus.trackName.contains(searchText) || mus.collectionName.contains(searchText)
                }
                    .collect(Collectors.toList())



            if (selectedOption.value == "1") {
                filter = filter.sortedBy {
                    it.trackName
                }
            } else {
                filter = filter.sortedBy {
                    it.collectionName
                }
            }

            muta_music.postValue(
                Result(
                    resultCount = cache_list.value!!.resultCount,
                    results = filter
                )
            )
        }
    }

    fun fetchMusic() {
        viewModelScope.launch {
            try {
                val response: Response<Result> = repository.getMusic()

                if (response.isSuccessful) {

                    muta_music.value = response.body()
                    cache_list.value = response.body()

                }else {
                        Log.d("fetch Music Failed", response.message().toString())
                }

            } catch (e: Exception) {
                Log.d("Repo", "fetch music exception ${e.message}")
            }
        }
    }

}