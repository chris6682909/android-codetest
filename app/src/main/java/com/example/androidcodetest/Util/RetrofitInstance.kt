package com.example.androidcodetest.Util

import android.util.Log
import com.example.androidcodetest.Service.ApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object RetrofitInstance {
    const val BASE_URL = "https://itunes.apple.com/"

    val api: ApiService by lazy {

        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(SimpleLoggingInterceptor())
            .build()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiService::class.java)
    }
}

class SimpleLoggingInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val request = chain.request()

        val response = chain.proceed(request)

        return response
    }
}