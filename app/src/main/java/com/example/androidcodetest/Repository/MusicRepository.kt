package com.example.androidcodetest.Repository

import android.util.Log
import com.example.androidcodetest.Util.RetrofitInstance
import retrofit2.Response
import com.example.androidcodetest.Models.Result

class MusicRepository {
    private val apiService = RetrofitInstance.api

    suspend fun getMusic(): Response<Result> {

        var response = apiService.getMusicsApi("Talyor+Swift","200","music")

        return response;
    }
}