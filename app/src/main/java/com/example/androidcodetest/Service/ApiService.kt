package com.example.androidcodetest.Service

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import com.example.androidcodetest.Models.Result

interface ApiService {
    @GET("search")
    suspend fun getMusicsApi(
        @Query(value = "term", encoded = true) term: String,
        @Query(value = "limit") limit: String,
        @Query(value = "media") media: String,
    ): Response<Result>
}