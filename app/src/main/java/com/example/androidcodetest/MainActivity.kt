package com.example.androidcodetest

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.androidcodetest.Models.Music
import com.example.androidcodetest.ViewModel.MusicViewModel
import com.example.androidcodetest.ui.theme.AndroidCodeTestTheme

class MainActivity : ComponentActivity() {

    private val viewModel: MusicViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AndroidCodeTestTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    viewModel.fetchMusic()

                    MusicList(viewModel)
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}


@Composable
fun MusicList(viewModel: MusicViewModel) {
    val musics by viewModel.live_result.observeAsState()

    Column {
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = viewModel.searchText, onValueChange = {
                viewModel.searchText = it
                viewModel.filterMusic()
            },
            label = { Text("Search by Song Name or Album Name") }
        )

        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            RadioButton(
                selected = viewModel.selectedOption.value == "1",
                onClick = {
                    viewModel.selectedOption.value = "1"
                    viewModel.filterMusic()
                }
            )
            Text("Sort by Song Name")

            RadioButton(
                selected = viewModel.selectedOption.value == "2",
                onClick = {
                    viewModel.selectedOption.value = "2"
                    viewModel.filterMusic()
                }
            )
            Text("Sort by Album Name")

        }

        if (!musics?.results.isNullOrEmpty()) {
            var muss = musics?.results as List<Music>

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                items(muss) { muss ->
                    Row {
                        AsyncImage(
                            model = muss.artworkUrl100,
                            contentDescription = "",
                            modifier = Modifier
                                .width(100.dp)
                                .height(100.dp)
                                .fillMaxSize()
                        )

                        Column {
                            Text(text = muss.trackName)
                            Text(text = muss.collectionName)
                        }
                    }
                    Divider()
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AndroidCodeTestTheme {
        Greeting("Android")
    }
}